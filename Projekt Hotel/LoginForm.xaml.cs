﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Projekt_Hotel
{
    /// <summary>
    /// Interaction logic for LoginForm.xaml
    /// </summary>
    public partial class LoginForm : Window
    {

        MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString); ///połączenie do bazy

        public LoginForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Logowanie
        /// Sprawdzenie w bazie występowania loginu i hasła podanego przez użytkownika. Jeśli istnieje logowanie do programu. Jeśli nie pojawaia sie komunikat
        /// </summary>
        public void Login(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("Select count(*) from uzytkownicy where login=@login AND haslo=@pass", conn);
                ///pobranie loginu od użytkownika
                cmd.Parameters.AddWithValue("@login", LoginBox.Text); ///pobranie loginu od użytkownika
                cmd.Parameters.AddWithValue("@pass", PassBox.Password); ///pobranie hasła od użytkownika
                PassBox.Clear();

                if (cmd.ExecuteScalar().ToString() == "1") ///sprawdzenie czy istnieje użytkownik z podanym hasłem - jeśli tak logowanie
                {
                    MainWindow window = new MainWindow();
                    window.Show();
                    this.Close();
                }
                else
                {
                    Message.Text = "Błędny login lub hasło"; ///wyswietlenie komunikatu o błędnym loginie/haśle
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            conn.Close();
        }
        /// <summary>
        /// Zamknięcie programu
        /// </summary>
        private void CancelClick(object sender, RoutedEventArgs e)
        {
                System.Windows.Application.Current.Shutdown(); ///zamknięcie programu

        }

    }
}
