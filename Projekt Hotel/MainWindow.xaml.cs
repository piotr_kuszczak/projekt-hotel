﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Projekt_Hotel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Ukrywa wszystkie gridy
        /// </summary>
        private void HideAllGrids()
        {
            OptionsGrid.Visibility = Visibility.Hidden; ///ukrywa grid
            AddOrderForm.Visibility = Visibility.Hidden;
            ClientsGrid.Visibility = Visibility.Hidden;
            RoomsGrid.Visibility = Visibility.Hidden;
            Services1Grid.Visibility = Visibility.Hidden;
            Services2Grid.Visibility = Visibility.Hidden;
            OrdersGridFilter.Visibility = Visibility.Hidden;
            ClientAdvancedSearch.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// wyświetla wszystkie rezerwacje
        /// </summary>
        public void OrdersClick(object sender, EventArgs e)
        {
            HideAllGrids(); ///wywołuje funkcję HideAllGrids
            OptionsGrid.Visibility = Visibility.Visible;///wyświetla grid dla rezerwacji
            try
            {
                conn.Open(); ///otwiera połączenie do bazy
                MySqlCommand cmd = new MySqlCommand("Select * from `wyswietl_wszystkie_rezerwacje`;", conn); ///wykonuje polecenie select
                DataTable dt = new DataTable(); ///tworzenie obiektu
                dt.Load(cmd.ExecuteReader()); ///ładowanie danych z bazy
                OrdersGrid.DataContext = dt; /// wyświetlenie danych
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            {
                conn.Close(); ///zamyka połączenie
            }
        }

        /// <summary>
        /// otwiera formularz dodania rezerwacji
        /// </summary>
        public void OpenForm(object sender, EventArgs e)
        {
            AddOrderForm.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// otwiera formularz dodania klienta
        /// </summary>
        public void OpenClientForm(object sender, EventArgs e)
        {
            AddClientForm.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// sprawdza czy pola nie są puste i dodaje rezerwację do bazy
        /// </summary>
        public void AddOrder(object sender, EventArgs e)
        {
            conn.Open(); ///otwiera połączenie
            using (MySqlCommand cmd = new MySqlCommand("insert into rezerwacje (`id_klienta`, `poczatek_pobytu`, `koniec_pobytu`, `id_pokoju`, `id_wyzywienia`, `id_pakietu`)" +
                " values(@client, @begin, @finish, @room, @srv1, @srv2)", conn)) ///wykonuje polecenie INSERT INTO 
            {
                cmd.Parameters.AddWithValue("@client", Client.Text); ///dodaje dane z pól tekstowych
                cmd.Parameters.AddWithValue("@begin", BeginDate.Text);
                cmd.Parameters.AddWithValue("@finish", FinishDate.Text);
                cmd.Parameters.AddWithValue("@room", RoomNumber.Text);
                cmd.Parameters.AddWithValue("@srv1", Service1.Text);
                cmd.Parameters.AddWithValue("@srv2", Service2.Text);

                if ((Client.Text.Trim() != string.Empty) ///sprawdza czy pola są uzupełnione
                    && (BeginDate.Text != string.Empty)
                    && (FinishDate.Text != string.Empty)
                    && (RoomNumber.Text != string.Empty)
                    && (Service1.Text != string.Empty)
                    && (Service2.Text != string.Empty)
                    )
                {
                    cmd.ExecuteNonQuery(); ///wykonanie polecenia
                    AddOrderForm.Visibility = Visibility.Hidden; ///ukrycie formularza
                    conn.Close(); ///zamknięcie połączenia
                }
                else
                {
                    OrderError.Text = "Uzupełnij poprawnie wszystkie pola!"; ///wyświetlenie komunikatu o błędzie użytkownika
                    conn.Close(); ///zamknięcie połączenia
                }

            }
        }

        /// <summary>
        /// zamyka formularze
        /// </summary>
        public void CloseForm(object sender, EventArgs e)
        {
            AddOrderForm.Visibility = Visibility.Hidden; ///zamyka formularz
            CheckIsFreeForm.Visibility = Visibility.Hidden;
            AdvancedSearch.Visibility = Visibility.Hidden;
            ClientAdvancedSearch.Visibility = Visibility.Hidden;
            AddClientForm.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// wyświetla wszystkich klientów
        /// </summary>
        public void ClientsClick(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            ClientsGrid.Visibility = Visibility.Visible;///wyświetla grid dla klientów

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("Select * from `klienci`;", conn); ///wykonuje polecenie select
                DataTable dt = new DataTable();///tworzenie obiektu
                dt.Load(cmd.ExecuteReader());///ładowanie danych z bazy
                ClientsDataGrid.DataContext = dt;///wyświetlenie danych

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }

            {
                conn.Close();///zamyka połączenie
            }
        }

        /// <summary>
        /// sprawdza czy wszystkie pola są uzupełnione i dodaje klienta do bazy
        /// </summary>
        public void AddClient(object sender, EventArgs e)
        {
            conn.Open();
            using (MySqlCommand cmd = new MySqlCommand("insert into klienci (`imie`, `nazwisko`, `adres_email`, `telefon`)" +
                " values(@fname, @lname, @email, @phone)", conn))///wykonuje polecenie INSERT INTO
            {
                cmd.Parameters.AddWithValue("@fname", Name.Text);///dodaje dane z pól tekstowych
                cmd.Parameters.AddWithValue("@lname", LastName.Text);
                cmd.Parameters.AddWithValue("@email", EmailAddress.Text);
                cmd.Parameters.AddWithValue("@phone", PhoneNumber.Text);
                if ((Name.Text.Trim() != string.Empty)///sprawdza czy pola są uzupełnione
                   && (LastName.Text != string.Empty)
                   && (EmailAddress.Text != string.Empty)
                   && (PhoneNumber.Text != string.Empty)
                   )
                {
                    cmd.ExecuteNonQuery();///wykonanie polecenia
                    AddOrderForm.Visibility = Visibility.Hidden;///ukrycie formularza
                    conn.Close();///zamknięcie połączenia
                }
                else
                {
                    ClientError.Text = "Uzupełnij poprawnie wszystkie pola!";///wyświetlenie komunikatu o błędzie użytkownika
                    conn.Close();///zamknięcie połączenia
                }

            }
        }

        /// <summary>
        /// wyświetla wszystkie pokoje
        /// </summary>
        public void RoomsClick(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            RoomsGrid.Visibility = Visibility.Visible;///wyświetla grid dla pokoi

            try
            {
                conn.Open();///otwiera połączenie do bazy
                MySqlCommand cmd = new MySqlCommand("Select * from `wszystkie_pokoje`;", conn);///wykonuje polecenie select
                DataTable dt = new DataTable();///tworzenie obiektu
                dt.Load(cmd.ExecuteReader());///ładowanie danych z bazy
                RoomsDataGrid.DataContext = dt;/// wyświetlenie danych

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }

            {
                conn.Close();///zamyka połączenie
            }
        }

        /// <summary>
        /// wyświetla wszystkie pakiety wyżywienia
        /// </summary>
        public void Services1Click(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            Services1Grid.Visibility = Visibility.Visible;///wyświetla grid dla pakietów

            try
            {
                conn.Open();///otwiera połączenie do bazy
                MySqlCommand cmd = new MySqlCommand("Select * from `pakiety_wyzywienia`;", conn);///wykonuje polecenie select
                DataTable dt = new DataTable();///tworzenie obiektu
                dt.Load(cmd.ExecuteReader());///ładowanie danych z bazy
                Services1DataGrid.DataContext = dt;///wyświetlenie danych

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }

            {
                conn.Close();///zamyka połączenie
            }
        }

        /// <summary>
        /// wyświetla wszystkie pakiety dodatków
        /// </summary>
        public void Services2Click(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            Services2Grid.Visibility = Visibility.Visible;///wyświetla grid dla pakietów

            try
            {
                conn.Open();///otwiera połączenie do bazy
                MySqlCommand cmd = new MySqlCommand("Select * from `pakiety_dodatkow`;", conn);///wykonuje polecenie select
                DataTable dt = new DataTable();///tworzenie obiektu
                dt.Load(cmd.ExecuteReader());///ładowanie danych z bazy
                Services2DataGrid.DataContext = dt;/// wyświetlenie danych

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }

            {
                conn.Close();///zamyka połączenie
            }
        }

        /// <summary>
        /// wylogowuje użytkownika. Otwiera ponownie okno logowania
        /// </summary>
        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Czy na pewno chcesz się wylogować?", "", System.Windows.MessageBoxButton.YesNo);///pyta o potwierdzenie
            if (messageBoxResult == MessageBoxResult.Yes)///sprawdza wybór użytkownika
            {
                LoginForm window = new LoginForm();///tworzy obiekt
                window.Show();///otwiera LoginForm
                this.Close();///zamyka MainWindow
            }
            if (messageBoxResult == MessageBoxResult.No)
            {

            }

        }

        /// <summary>
        /// zamyka program
        /// </summary>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Czy na pewno chcesz zamknać?", "", System.Windows.MessageBoxButton.YesNo);///pyta o potwierdzenie
            if (messageBoxResult == MessageBoxResult.Yes)///sprawdza wybór użytkownika
            {
                System.Windows.Application.Current.Shutdown();///zamyka aplikację

            }
            if (messageBoxResult == MessageBoxResult.No)
            {

            }
        }

        /// <summary>
        /// wyświetla formularz filtrowania rezerwacji
        /// </summary>
        public void OpenFilter(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            AdvancedSearch.Visibility = Visibility.Visible;///otwiera filtry
        }

        /// <summary>
        /// wyświetla formularz filtrowania klientów
        /// </summary>
        public void OpenClientFilter(object sender, EventArgs e)
        {
            HideAllGrids();///wywołuje funkcję HideAllGrids
            AdvancedSearch.Visibility = Visibility.Visible;
            ClientsGrid.Visibility = Visibility.Visible;
            ClientsDataGrid.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// wyszukuje rezerwacje według podanych wartości
        /// </summary>
        private void FindOrder(object sender, RoutedEventArgs e)
        {
            try
            {
                conn.Open();///otwarcie połączenia
                string Query = "Select * from `wyswietl_wszystkie_rezerwacje`";///szablon polecenia
                MySqlCommand cmd1 = new MySqlCommand(Query, conn);
                
                List<string> conditions = new List<string>(); ///utworzenie obiektu
                if (OrderNr.Text.Trim() != string.Empty)///sprawdzenie czy pole nie jest puste
                {
                    string orderF = string.Concat("Numerrezerwacji = ", OrderNr.Text); ///złączenie tekstu - nazwy kolumny w bazie z wartością wpisaną przez użytkownika
                    conditions.Add(orderF);///przekazuje orderF do conditions
                }
                if (NameF.Text.Trim() != string.Empty)
                {
                    string nameF = string.Concat("Imie = ", NameF.Text);
                    conditions.Add(nameF);
                }
                if (LName.Text.Trim() != string.Empty)
                {
                    string lnameF = string.Concat("Nazwisko = ", LName.Text);
                    conditions.Add(lnameF);
                }
                if (BeginF.Text.Trim() != string.Empty)
                {
                    string beginF = string.Concat("Przyjazd = ", BeginF.Text);
                    conditions.Add(beginF);
                }
                if (EndF.Text.Trim() != string.Empty)
                {
                    string endF = string.Concat("Wyjazd = ", EndF.Text);
                    conditions.Add(endF);
                }
                if (RoomF.Text.Trim() != string.Empty)
                {
                    string roomF = string.Concat("Numerpokoju = ", RoomF.Text);
                    conditions.Add(roomF);
                }
                if (conditions.Any())
                {
                    Query += " WHERE " + string.Join(" AND ", conditions.ToArray());///dodaje do szablonowego polecenia warunki
                }

                cmd1.CommandText = Query; ///przeładowanie obiektu
                DataTable dt1 = new DataTable(); ///utworzenie obiektu
                dt1.Load(cmd1.ExecuteReader()); ///załadowanie danych
                OrdersGridFilter.DataContext = dt1; ///wyświetlenie danych
                OrdersGrid.Visibility = Visibility.Hidden;//ukrycie filtrów
                AdvancedSearch.Visibility = Visibility.Hidden;
                OrdersGridFilter.Visibility = Visibility.Visible;///wyświetlenie gridu z wynikiem
            }
            catch (MySqlException ex)
            {
            }

            {
                conn.Close();///zamknięcie połączenia
            }
        }

        /// <summary>
        /// wyszukuje klientów według podanych wartości
        /// </summary>
        private void FindClient(object sender, RoutedEventArgs e)
        {
            try
            {
                conn.Open();///otwarcie połączenia
                string Query = "Select * from `klienci`";///szablon polecenia
                MySqlCommand cmd1 = new MySqlCommand(Query, conn);

                List<string> conditions = new List<string>();///utworzenie obiektu
                if (CnameF.Text.Trim() != string.Empty)///sprawdzenie czy pole nie jest puste
                {
                    string nameF = string.Concat("Imie = ", CnameF.Text);///złączenie tekstu - nazwy kolumny w bazie z wartością wpisaną przez użytkownika
                    conditions.Add(nameF);///przekazuje orderF do conditions
                }
                if (CEmailF.Text.Trim() != string.Empty)
                {
                    string emailF = string.Concat("Nazwisko = ", CEmailF.Text);
                    conditions.Add(emailF);
                }
                if (LName.Text.Trim() != string.Empty)
                {
                    string lnameF = string.Concat("adres_email = ", LName.Text);
                    conditions.Add(lnameF);
                }
                if (BeginF.Text.Trim() != string.Empty)
                {
                    string beginF = string.Concat("Telefon = ", BeginF.Text);
                    conditions.Add(beginF);
                }
                if (CIdF.Text.Trim() != string.Empty)
                {
                    string idF = string.Concat("id_klienta = ", CIdF.Text);
                    conditions.Add(idF);
                }
                if (conditions.Any())
                {
                    Query += " WHERE " + string.Join(" AND ", conditions.ToArray());///dodaje do szablonowego polecenia warunki
                }

                cmd1.CommandText = Query;///przeładowanie obiektu
                DataTable dt1 = new DataTable();///utworzenie obiektu
                dt1.Load(cmd1.ExecuteReader());///załadowanie danych
                ClientsDataGrid.DataContext = dt1;///wyświetlenie danych
                ClientAdvancedSearch.Visibility = Visibility.Hidden;///zamknięcie okna filtrów
            }
            catch (MySqlException ex)
            {
            }

            {
                conn.Close();///zamknięcie połączenia
            }
        }

        /// <summary>
        /// otwiera okno sprawdzenia dostępności
        /// </summary>
        public void OpenCheckForm(object sender, EventArgs e)
        {
            HideAllGrids();///otwarcie połączenia
            CheckIsFreeForm.Visibility = Visibility.Visible;///otwiera formularz sprawdzania dostępności
        }

        /// <summary>
        /// sprawdza czy pokój jest dostępny
        /// </summary>
        private void CheckIsFree(object sender, RoutedEventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select count(*) from `wyswietl_wszystkie_rezerwacje` " +
                    "WHERE Przyjazd>=@begin AND Wyjazd<=@end " +
                    "AND Numerpokoju=@room", conn);///wykonuje polecenie select
                cmd.Parameters.AddWithValue("@begin", BeginC.Text);///dodaje dane z pól tekstowych do polecenia
                cmd.Parameters.AddWithValue("@end", EndC.Text);
                cmd.Parameters.AddWithValue("@room", RoomC.Text);
                if (cmd.ExecuteScalar().ToString() == "0") ///sprawdza czy w bazie pojawił znajduje się rekord dotyczący rezerwacji 
                {
                    Result.Foreground = new SolidColorBrush(Colors.Green);
                    Result.Text = "Pokój jest wolny"; ///wyświetla komunikat
                }
                else
                {
                    Result.Text = "Pokój jest zajęty";///wyświetla komunikat
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());

            }

            {
                conn.Close(); ///zamyka połączenie
            }
        }
    
    }
}

